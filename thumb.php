<?php

require_once __DIR__ . '/vendor/autoload.php';

$image = new \Kisphp\ImageResizer();

$image->load(__DIR__ . '/image-big.jpg');

$image->setTarget(__DIR__ . '/image-small.jpg');

$image->resize(300, 0);

$image->save();